"""Licensed under GPL v3 by Professor Code."""

import csv
from pygame.math import Vector2
from pgzero.actor import Actor


class Map:
    def __init__(self):
        self.tiles = set()
        self.springs = set()
        self.TILE_SIZE = Vector2(21, 21)
        self.MAP_SIZE = Vector2(self.TILE_SIZE.x * 50, self.TILE_SIZE.y * 30)
        level = self.get_data('levels/level1.csv')
        self.create_map(level)

    def get_data(self, level_data):
        with open(level_data) as f:
            level = list(csv.reader(f))

        return level

    def create_map(self, level):
        for y, row in enumerate(level):
            for x, cell in enumerate(row):
                position = x * self.TILE_SIZE.x, y * self.TILE_SIZE.y
                if not int(cell) == 900 and not int(cell) == -1:
                    name = 'tile_' + cell.rjust(4, '0')
                    tile = Actor(name, topleft=position)
                    if int(cell) == 284:
                        tile.launched = False
                        self.springs.add(tile)
                    elif int(cell) == 253:
                        self.exit = tile
                    else:
                        self.tiles.add(tile)
                elif int(cell) == 900:
                    self.player_start = position

    def draw(self, display):
        for tile in self.tiles:
            display.blit(tile._surf, tile.topleft)
        display.blit(self.exit._surf, self.exit.topleft)
