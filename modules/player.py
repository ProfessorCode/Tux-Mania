"""Licensed under GPL v3 by Professor Code."""

from pygame.math import Vector2
from pygame.transform import flip
from pgzero.actor import Actor
from pgzero.keyboard import keyboard, keys
from pgzero.builtins import sounds
import json


class Player:
    def __init__(self, map):
        self.image = Actor('tux_20')
        self.image.topleft = map.player_start
        self.tiles = map.tiles
        self.map = map
        self.direction = Vector2(0, 0)
        self.speed = 2
        self.gravity = 0.8
        self.jump_height = -10
        self.acceleration = 1.05
        self.deceleration = 0.97
        self.max_speed = 7
        self.on_ground = False
        self.animations = json.load(open('data/player_animations.json'))
        self.state = 'run'
        self.current_frame = 0
        self.offset = Vector2()
        self.flip_image = False
        self.moving_right = False
        self.moving_left = False
        self.staying_still = False

    def get_input(self):
        if keyboard.right:
            self.break_slide()
            if self.moving_left:
                self.deceleration = 0.90
                self.decrease_speed()
            else:
                self.direction.x = 1
                self.increase_speed()
                self.flip_image = False
                self.moving_right = True
        elif keyboard.left:
            self.break_slide()
            if self.moving_right:
                self.deceleration = 0.90
                self.decrease_speed()
            else:
                self.direction.x = -1
                self.increase_speed()
                self.flip_image = True
                self.moving_left = True
        else:
            self.staying_still = True
            self.deceleration = 0.97
            self.decrease_speed()

    def move(self):
        self.get_input()
        self.apply_gravity()
        self.image.y += self.direction.y
        self.vertical_collision()
        self.level_scroll()
        self.image.x += self.direction.x * self.speed
        self.horizontal_collision()

    def apply_gravity(self):
        self.direction.y += self.gravity

    def vertical_collision(self):
        for tile in self.tiles:
            on_tile = self.is_on_tile(tile)
            if tile.colliderect(self.image):
                if on_tile:
                    self.image.bottom = tile.top
                    self.direction.y = 0
                    self.on_ground = True
                elif tile.collidepoint(self.image.midtop):
                    self.image.top = tile.bottom
                    self.direction.y = 0

    def horizontal_collision(self):
        for tile in self.tiles:
            if self.image.collidepoint(tile.midleft):
                self.image.right = tile.left
            elif self.image.collidepoint(tile.midright):
                self.image.left = tile.right

    def jump(self, key):
        if key == keys.SPACE and self.on_ground:
            sounds.jump.play()
            self.direction.y += self.jump_height
            self.on_ground = False

    def level_scroll(self):
        self.offset.x = self.map.MAP_SIZE.x - \
            (self.map.MAP_SIZE.x + self.image.x)
        self.offset.y = -self.map.MAP_SIZE.y + \
            (self.map.MAP_SIZE.y - self.image.y - 50)

    def increase_speed(self):
        if self.speed < self.max_speed:
            self.speed *= self.acceleration

    def animate(self):
        self.image.image = self.animations[self.state][self.current_frame]
        frames = len(self.animations[self.state])

        if self.flip_image:
            self.image._surf = flip(self.image._surf, True, False)

        if self.current_frame < frames - 1 and self.direction.x != 0:
            self.current_frame += 1
        elif self.current_frame == frames - 1 and self.direction.x != 0:
            self.current_frame = frames - 1
        else:
            self.current_frame = 0

    def decrease_speed(self):
        self.speed *= self.deceleration
        if self.speed <= 2:
            self.direction.x = 0
            self.speed = 2
            self.moving_left = False
            self.moving_right = False
            self.state = 'run'
            self.current_frame = 0
        elif self.speed >= 4:
            self.state = 'slide'
            self.current_frame = 0

    def is_on_tile(self, tile):
        if tile.collidepoint(self.image.midbottom):
            on_tile = True
        elif tile.collidepoint(self.image.bottomleft):
            on_tile = True
        elif tile.collidepoint(self.image.bottomright):
            on_tile = True
        else:
            on_tile = False
        return on_tile

    def break_slide(self):
        if self.staying_still:
            self.staying_still = False
            self.state = 'run'
            self.current_frame = 0

    def make_footsteps(self):
        if self.state == 'run' and self.current_frame > 0:
            if self.on_ground and self.current_frame < 16:
                sounds.footsteps.play()
