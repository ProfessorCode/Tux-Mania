"""Licensed under GPL v3 by Professor Code."""

from pgzero.builtins import sounds
from pgzero.clock import clock


class Objects:
    def __init__(self, springs):
        self.springs = springs
        clock.schedule_interval(self.animate, 0.2)

    def interact(self, player):
        self.bounce(player)

    def draw(self, display):
        for spring in self.springs:
            display.blit(spring._surf, spring.topleft)

    def bounce(self, player):
        for spring in self.springs:
            on_spring = self.is_on_spring(spring, player)
            if on_spring:
                player.direction.y = -15
                player.on_ground = False
                sounds.bounce.play()
                spring.launched = True

    def animate(self):
        for spring in self.springs:
            if spring.launched:
                spring.image = 'tile_0283'
                spring.launched = False
            else:
                spring.image = 'tile_0284'

    def is_on_spring(self, spring, player):
        if spring.collidepoint(player.image.midbottom):
            on_spring = True
        elif spring.collidepoint(player.image.bottomleft):
            on_spring = True
        elif spring.collidepoint(player.image.bottomright):
            on_spring = True
        else:
            on_spring = False

        return on_spring
