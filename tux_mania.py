"""Licensed under GPL v3 by Professor Code."""

import pgzrun
from modules.make_map import Map
from modules.player import Player
from modules.objects import Objects
from pygame.transform import scale2x
from pygame.surface import Surface
from pgzero.screen import Screen
from sys import exit


def draw():
    screen.clear()
    game.display.clear()
    screen.fill('#88c2f6')
    game.display.fill('#88c2f6')
    game.draw()
    display = scale2x(game.display.surface)
    screen.blit(display, game.player.offset)


def update():
    game.player.move()
    game.player.animate()
    game.objects.interact(game.player)
    game.reached_exit()


def on_key_down(key):
    game.player.jump(key)


class TuxMania:
    def __init__(self):
        self.map = Map()
        self.player = Player(self.map)
        surface = Surface(self.map.MAP_SIZE)
        self.display = Screen(surface)
        self.objects = Objects(self.map.springs)
        music.play('level_music')
        music.set_volume(0.3)
        clock.schedule_interval(self.player.make_footsteps, 0.2)

    def draw(self):
        self.map.draw(self.display)
        self.objects.draw(self.display)
        self.display.blit(self.player.image._surf, self.player.image.topleft)

    def reached_exit(self):
        if game.player.image.colliderect(game.map.exit):
            clock.schedule(exit, 0.2)


game = TuxMania()
WIDTH, HEIGHT = game.map.MAP_SIZE
TITLE = 'Tux Mania'
ICON = 'images/tux_00.png'
pgzrun.go()
