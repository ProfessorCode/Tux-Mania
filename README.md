# Tux Mania
A speed-based platformer made for Linux Game Jam 2022 inspired by *Extreme Tux Racer* and *Sonic* games.


## Running The Game
Requires a Linux distribution with `glibc >= 2.14`. (Ubuntu 20.04 or higher)

1. Download the game from [itch.io](https://professorcode.itch.io/tux-mania). 
2. Extract the ZIP file.
3. Make sure `tux_mania` is executable.
4. Run the executable.

## Dependencies
For running from source, it is recommended to have the following:
```
python >= 3.8
pgzero >= 1.2.1
```

## Copyright Attributions
### Graphics
- Tux - [Kelvin Shadewing](https://opengameart.org/content/tux-kyrodian-legends-style)
- Almost Everything Else - [Kenney](https://kenney.nl/assets/platformer-art-pixel-redux)

### Music and Sounds
- Jumping, Running and Bouncing Effects - [SubspaceAudio](https://opengameart.org/content/512-sound-effects-8-bit-style)
- Background Soundtrack - [Patrick De Arteaga](https://patrickdearteaga.com/)

## License
This game has been licensed under **GPL v3**. Please refer to `LICENSE` for more information.
